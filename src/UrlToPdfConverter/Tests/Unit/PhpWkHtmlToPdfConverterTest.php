<?php

namespace ArashDastafshan\UrlToPdfConverter\UrlToPdfConverter\Tests\Unit;

use ArashDastafshan\UrlToPdfConverter\UrlToPdfConverter\PhpWkHtmlToPdfConverter;

/**
 * Tests PhpWkHtmlToPdfConverter
 * Class PhpWkHtmlToPdfConverterTest.
 */
class PhpWkHtmlToPdfConverterTest extends \PHPUnit_Framework_TestCase
{
    public function testExecute()
    {
        $wkHtmlToPdfMock = $this->createWkHtmlToPdfMock();

        $wkHtmlToPdfMock->shouldReceive('saveAs')
            ->once()
            ->andReturnTrue();

        $phpWkHtmlToConverter = new PhpWkHtmlToPdfConverter(
            'google.com',
            '/testlocation',
            $wkHtmlToPdfMock
        );

        $phpWkHtmlToConverter->execute();
    }

    /**
     * @expectedException \Exception
     */
    public function testExecuteError()
    {
        $wkHtmlToPdfMock = $this->createWkHtmlToPdfMock();

        $wkHtmlToPdfMock->shouldReceive('saveAs')
            ->once()
            ->andReturnFalse();

        $wkHtmlToPdfMock->shouldReceive('getError')
            ->andReturn('Cannot create PDF as expected.');

        $phpWkHtmlToConverter = new PhpWkHtmlToPdfConverter(
            'google.com',
            '/testlocation',
            $wkHtmlToPdfMock
        );

        $phpWkHtmlToConverter->execute();
    }

    /**
     * @return \Mockery\MockInterface|\mikehaertl\wkhtmlto\Pdf
     */
    private function createWkHtmlToPdfMock()
    {
        return \Mockery::mock('mikehaertl\wkhtmlto\Pdf')
            ->shouldReceive('addPage')
            ->once()
            ->andReturnSelf()
            ->getMock();
    }
}
