<?php

namespace ArashDastafshan\UrlToPdfConverter\UrlToPdfConverter;

/**
 * Interface UrlToPdfConverterInterface.
 */
interface UrlToPdfConverterInterface
{
    /**
     * Executes the  URL to PDF conversion and saves it to a predefined location.
     *
     * @throws \Exception
     */
    public function execute();
}
