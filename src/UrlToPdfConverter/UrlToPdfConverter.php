<?php

namespace ArashDastafshan\UrlToPdfConverter\UrlToPdfConverter;

use ArashDastafshan\SymfonyValidatorConstraints\Symfony\Component\Validator\Constraints\UrlWithOptionalProtocol;
use Symfony\Component\Validator\Mapping\ClassMetadata;

abstract class UrlToPdfConverter implements UrlToPdfConverterInterface
{
    protected $url;
    protected $savePath;

    /**
     * UrlToPdfConverter constructor.
     *
     * @param string $url
     * @param string $savePath
     */
    public function __construct(string $url, string $savePath)
    {
        $this->url = $url;
        $this->savePath = $savePath;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint(
            'url',
            new UrlWithOptionalProtocol(
                ['message' => 'The given URL is not valid.']
            )
        );
    }
}
