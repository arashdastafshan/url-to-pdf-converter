<?php

namespace ArashDastafshan\UrlToPdfConverter\UrlToPdfConverter;

use mikehaertl\wkhtmlto\Pdf;

/**
 * Class PhpWkHtmlToPdfGenerator.
 */
class PhpWkHtmlToPdfConverter extends UrlToPdfConverter
{
    /**
     * @var Pdf
     */
    private $wkHtmlToPdf;

    /**
     * PhpWkHtmlToPdfGenerator constructor.
     *
     * @param string $url
     * @param string $savePath
     * @param Pdf    $wkHtmlToPdf
     */
    public function __construct(string $url, string $savePath, Pdf $wkHtmlToPdf)
    {
        parent::__construct($url, $savePath);

        $this->wkHtmlToPdf = $wkHtmlToPdf;
    }

    public function execute()
    {
        $this->wkHtmlToPdf->addPage($this->url);

        if (false === $this->wkHtmlToPdf->saveAs($this->savePath)) {
            throw new \Exception(
                'Could not create PDF: '.$this->wkHtmlToPdf->getError()
            );
        }
    }
}
